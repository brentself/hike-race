<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/blog', function () {
    return view('article/blog');
});

Route::get('/login', function () {
    return view('login');
});

Route::resources([
    'articles' => 'ArticleController',
    'hikes' => 'HikeController',
    'users' => 'UserController'
]);

Route::prefix('user')->group(function () {
    Route::get('/', function () {
            // Matches The "/admin/users" URL
            return view('user/dashboard', ['articles' => [0, 1], 'hikes' => [1, 2], 'users' => [2, 3]]);
        });
});

Route::prefix('admin')->group(function () {
    Route::namespace('admin')->group(function () {
        Route::get('/', function () {
            // Matches The "/admin/users" URL
            return view('admin/dashboard', ['articles' => [0, 1], 'hikes' => [1, 2], 'users' => [2, 3]]);
        });
        Route::resources([
            'admin/articles' => 'AdminArticleController',
            'admin/hikes' => 'AdminHikeController',
            'users' => 'AdminUserController'
        ]);
    });
});
