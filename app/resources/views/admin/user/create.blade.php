@extends('layouts.admin')

@section('title', 'Users')

@section('content')
<h1>Create User</h1>
<div class="row">
    <form method="POST" action="/admin/users" class="needs-validation">
        @csrf
        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="firstName">First name</label>
                <input type="text" class="form-control" id="first" name="first" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <label for="lastName">Last name</label>
                <input type="text" class="form-control" id="last" name="last" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" value="" required="">
            <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
            </div>
        </div>
        <div class="mb-3">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
            <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
            </div>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Create User</button>
    </form>
</div>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush