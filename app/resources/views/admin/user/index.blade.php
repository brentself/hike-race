@extends('layouts.admin')

@section('title', 'Users')

@section('content')
<h1>Users</h1>
<div class="row">
    <div class="col-12"><a href="{{ url('admin/users/create') }}">+ Create User</a></div>
</div>
<table class="table list-unstyled">
    <thead>
        <tr>
            <th>Email</th>
            <th>Last</th>
            <th>First</th>
            <th>Created</th>
            <th>Updated</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->email }}</td>
            <td>{{ $user->last }}</td>
            <td>{{ $user->first }}</td>
            <td>{{ $user->created_at }}</td>
            <td>{{ $user->updated_at }}</td>
            <td><a href="{{ url('admin/users/' . $user->id . '/edit') }}">Edit</a></td>
            <td><form method="POST" action="{{ url('admin/users', $user) }}">@method('DELETE')@csrf<a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Delete</a></form></td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush