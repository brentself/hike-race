@extends('layouts.app')

@section('title', 'Hikes')

@section('content')
<h1>Hikes</h1>
<p>
    <ul class="list-unstyled">
        @foreach ($hikes as $hike)
        <li>{{ $hike }}</li>
        @endforeach
    </ul>
</p>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush