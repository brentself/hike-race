@extends('layouts.admin')

@section('title', 'Articles')

@section('content')
<h1>Articles</h1>
<p>
    <ul class="list-unstyled">
        @foreach ($articles as $article)
        <li>{{ $article }}</li>
        @endforeach
    </ul>
</p>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush