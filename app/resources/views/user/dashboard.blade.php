@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<h1>Dashboard</h1>
<div class="row">
    <div class="col-md-4">
        <h1>Recent Hikes</h1>
        <p>
            <ul class="list-unstyled">
                @foreach ($hikes as $hike)
                <li>{{ $hike }}</li>
                @endforeach
            </ul>
        </p>
    </div>
    <div class="col-md-4">
        <h1>Recent Articles</h1>
        <p>
            <ul class="list-unstyled">
                @foreach ($articles as $article)
                <li>{{ $article }}</li>
                @endforeach
            </ul>
        </p>
    </div>
</div>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush