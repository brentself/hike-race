@extends('layouts.app')

@section('title', 'Users')

@section('content')
<h1>Login</h1>
<div class="row">
    <form method="POST" action="/authenticate" class="needs-validation">
        @csrf
        <div class="mb-3">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" value="" required="">
            <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
            </div>
        </div>
        <div class="mb-3">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
            <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
            </div>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
    </form>
</div>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush