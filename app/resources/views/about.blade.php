@extends('layouts.app')

@section('title', 'Hikes')

@section('content')
<h1>About Us</h1>
<div class="row">
    <div class="col-md-4">
        <h2>Hikes</h2>
        <p>
            Hike events are "races" that anyone can sponsor.
        </p>
    </div>
    <div class="col-md-4">
        <h2>Charities</h2>
        <p>
            Hike Races are a great way to raise money and awareness 
            for charities...and to have a lot of fun!
        </p>
    </div>
    <div class="col-md-4">
        <h2>Team Building</h2>
        <p>
            Host a hike for your office or organization
        </p>
    </div>
</div>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush