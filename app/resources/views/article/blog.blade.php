@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<h1>Hike Race Blog</h1>
<div class="row">
    <div class="col-md-4">
        <p>
            @isset($articles)
            <ul class="list-unstyled">
                @foreach ($articles as $article)
                <li>
                    <h2>{{ $article->title }}</h2>
                    <p>{{ $article->content }}</p>
                </li>
                @endforeach
            </ul>
            @endisset

            @empty($articles)
                No articles found.
            @endempty
        </p>
    </div>
</div>
@endsection

@push('scripts')
    <!-- <script src="/js/hike.js"></script> -->
@endpush