<!doctype html>
    <head>
        <title>Hike Race - @yield('title')</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="robots" content="index, follow">
        <meta name="keywords" content="Hiking Events and Races">
        <meta name="description" content="Hiking Events and Races">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="/favicon.ico">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <header>
                <div class="bg-dark collapse" id="navbarHeader">
                    <div class="container">
                      <div class="row">
                        <div class="col-sm-4 offset-md-1 py-4">
                            <!-- <h4 class="text-white">Contact</h4> -->
                            <ul class="list-unstyled">
                                <li><a href="/hikes" class="text-white">Hike Events</a></li>
                                <li><a href="/about" class="text-white">About</a></li>
                                <li><a href="/contact" class="text-white">Contact</a></li>
                                @if (Route::has('login'))
                                    <div class="top-right links">
                                        @auth
                                            <li><a href="{{ url('/home') }}">Home</a></li>
                                        @else
                                            <li><a href="{{ route('login') }}">Login</a></li>

                                            @if (Route::has('register'))
                                                <li><a href="{{ route('register') }}">Register</a></li>
                                            @endif
                                        @endauth
                                    </div>
                                @endif
                            </ul>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="navbar navbar-dark bg-dark shadow-sm">
                    <div class="container d-flex justify-content-between">
                        <a class="navbar-brand d-flex align-items-center" href="/">Hike Race</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                </div>
            </header>

            <div class="container">
                @yield('content')
            </div>
        </div>

        @push('scripts')
            <script src="/js/app.js"></script>
        @endpush
        
        @stack('scripts')
    </body>
</html>