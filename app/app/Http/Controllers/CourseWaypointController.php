<?php

namespace App\Http\Controllers;

use App\CourseWaypoint;
use Illuminate\Http\Request;

class CourseWaypointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseWaypoint  $courseWaypoint
     * @return \Illuminate\Http\Response
     */
    public function show(CourseWaypoint $courseWaypoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseWaypoint  $courseWaypoint
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseWaypoint $courseWaypoint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseWaypoint  $courseWaypoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseWaypoint $courseWaypoint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseWaypoint  $courseWaypoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseWaypoint $courseWaypoint)
    {
        //
    }
}
