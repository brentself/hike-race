# Concepts
Hiking as a race.

###Race sponsor schedules an event.
* invite emails and/or text messages sent to user list (spreadsheet?)
* reminder email/text sent approaching

###Contestants register on the website or through the mobile app.

###Contestants check-in on the event day with mobile device or race official.

###Contestants using the mobile app broadcast their location to the website after the race begins.
* send time when passing checkpoint (max of 23) is recorded/used for points/metrics calculation
* calculate speed (distance of location broadcasts vs time) is graphed (for user only or all?)
* if position does not change for a certain period of time, official/rescue dispatched
* biometrics monitoring (opt-in)
* realtime locations placed on map (opt-in)
* contestants can subscribe to alerts (when users cross checkpoints or course end)

#Application Structure

## Race Events
* date and time
* fee
* prize
* participation requirements
* event terms & conditions, disclaimer
* participants
* check-in pack
* course
	* start location
	* checkpoints/waypoints
	* end (can be same as start, if waypoints exist)

## Users
* race admin
* race sponsor (same as admin or client)
* race official
* race hiker/contestant

## API Routes
* /api/user GET (SELECT *)
* /api/user/{id} GET (SELECT by id LIMIT 1)
* /api/user POST (INSERT)
* /api/user/{id} PUT (UPDATE)
* /api/user/{id} DELETE (DELETE)
* /api/role
* /api/course
* /api/waypoint
* /api/hike
* /api/article
## Web Routes
* /home
* /about
* /contact
* /blog
* /blog/{article-name}
* /hike (calendar)
* /hike/schedule-hike
* /hike/{hike-name} (description, hikers, live map)
* /admin (dashboard)
* /admin/user
* /admin/user/create
* /admin/user/edit
* /admin/hike
* /admin/hike/create
* /admin/hike/edit
* /admin/article
* /admin/article/create
* /admin/article/edit
* /login
* /user (dashboard)
* /user/create (admin, sponsor, official)
* /user/recover
* /user/hike
