# install laravel through the composer container
docker run --rm --interactive --tty --volume $PWD/app:/app composer global require laravel/installer && composer create-project --prefer-dist laravel/laravel app
# make the logs directory writable on the volume's host machine (macOS only?)
chmod -Rf 0777 ./app/storage
# start the containers
docker-compose up -d
# create the model and migration files
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:model Role -m
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:model Course -m
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:model Waypoint -m
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:model CourseWaypoint -m
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:model Hike -m
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:model HikeUser -m
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:model Article -m
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan migrate
# create the controllers with CRUD routes
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:controller RoleController --resource --model=Role
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:controller UserController --resource --model=User
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:controller CourseController --resource --model=Course
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:controller CourseWaypointController --resource --model=CourseWaypoint
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:controller HikeController --resource --model=Hike
docker exec -t -i hike_php7.3.3_alpine3.9 /var/www/html/artisan make:controller ArticleController --resource --model=Article
# install npm dependencies (for sass, vue, jquery, etc.)
# npm not in nginx image/container, no official image, use local version
npm install && npm run dev
# open mysql command line client
docker run -it --network web_hike-network --rm mysql:5.7 mysql -hhike_mysql5.7 -uhike_race -p
# open adminer
# http://192.168.99.100:8080
docker run --network web_hike-network --link hike_mysql5.7:db -p 8080:8080 adminer
#stop the containers
docker-compose down